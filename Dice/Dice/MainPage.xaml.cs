﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Dice
{
	public partial class MainPage : ContentPage
	{
        List<Die> dice;
        Label col1, col2;
        public MainPage()
        {

            InitializeComponent();
            col1 = new Label
            {
                Text = "Die #",
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White
            };
            col2 = new Label
            {
                Text = "Rolled #",
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White
            };

            ListView lv = new ListView();

            Button rollAllButton = new Button
            {
                Command = new Command(() => RollAll()),
                Text = "Roll All"
            };

            dice = new List<Die>
            {
                new Die(4),
                new Die(6),
                new Die(8),
                new Die(10),
                new Die(12),
                new Die(20)
            };

            lv.ItemTemplate = new DataTemplate(typeof(DieCell));

            lv.ItemsSource = dice;
            Grid grid = new Grid
            {
                BackgroundColor = Color.Gray
            };

            grid.ColumnDefinitions.Add(new ColumnDefinition
            {
                Width = GridLength.Star
            });

            grid.ColumnDefinitions.Add(new ColumnDefinition
            {
                Width =  GridLength.Star
            });

            grid.ColumnDefinitions.Add(new ColumnDefinition
            {
                Width = GridLength.Star
            });

            grid.RowDefinitions.Add(new RowDefinition
            {
                Height = GridLength.Auto
            });
            grid.RowDefinitions.Add(new RowDefinition
            {
                Height = GridLength.Auto,
            });

            grid.Children.Add(col1, 0, 0);
            grid.Children.Add(col2, 1, 0);
            grid.Children.Add(rollAllButton, 2, 0);
            grid.Children.Add(lv, 0, 1);

            Grid.SetColumnSpan(lv, 3);

            Content = grid;
        }

        void RollAll()
        {
            foreach(Die d in dice)
            {
                d.RollDie();
            }
        }

    }

    public class DieCell : ViewCell
    {
        public DieCell()
        {
            Grid diceGrid = new Grid();

            Label die = new Label
            {
                HorizontalTextAlignment = TextAlignment.Center
            };
            Label val = new Label
            {
                HorizontalTextAlignment = TextAlignment.Center
            };
            Button roller = new Button
            {
                Text = "Roll",
                HorizontalOptions = LayoutOptions.Center
            };

            die.SetBinding(Label.TextProperty, "name");
            val.SetBinding(Label.TextProperty, "val");
            val.SetBinding(Label.TextColorProperty, "color");
            roller.SetBinding(Button.CommandProperty, "Roll");
            die.TextColor = Color.White;

            diceGrid.BackgroundColor = Color.Gray;


            diceGrid.ColumnDefinitions.Add(new ColumnDefinition
            {
                Width = GridLength.Star
            });

            diceGrid.ColumnDefinitions.Add(new ColumnDefinition
            {
                Width = GridLength.Star
            });

            diceGrid.ColumnDefinitions.Add(new ColumnDefinition
            {
                Width = GridLength.Star
            });

            diceGrid.RowDefinitions.Add(new RowDefinition
            {
                Height = GridLength.Star
            });

            diceGrid.Children.Add(die, 0, 0);
            diceGrid.Children.Add(val, 1, 0);
            diceGrid.Children.Add(roller, 2, 0);

            View = diceGrid;
        }

    }

    public class Die : INotifyPropertyChanged
    {
        private int _max;
        private int _val;
        private Color _color;

        public string name
        {
            get;
            set;
        }

        public Color color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                OnPropertyChanged("color");
            }
        }

        public int max
        {
            get { return _max; }
            set { _max = value; }
        }

        public int val
        {
            get { return _val; }
            set
            {
                _val = value;
                OnPropertyChanged("val");
            }
        }

        public Command Roll
        {
            get
            {
                return new Command(() => RollDie());
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Die(int maximum)
        {
            max = maximum;
            val = 1;
            name = "d" + maximum;
            color = Color.White;
        }

        public void RollDie()
        {
            val = new Random().Next(1, max + 1);
            color = color.Equals(Color.White) ? Color.LightBlue : Color.White;
        }

        protected virtual void OnPropertyChanged(string sender)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(sender));
        }
    }

}
